require "selenium-webdriver"
require 'uri'
require 'oga'
require 'csv'
require 'logger'
require 'securerandom'

class PinnacleBetter
  def initialize(config)
    log_dir = config['log_dir']
    FileUtils.mkdir_p(log_dir) unless FileTest.exist?(log_dir)
    @logger = Logger.new(File.join(log_dir, "better_#{Time.now.strftime("%Y%m%d%H%M%S")}.log"))

    @bet_history = CSV.open('bet_history.csv', 'a')
    @bet_history.sync = true

    @id = config['pinnacle']['account']['id']
    @pw = config['pinnacle']['account']['pw']
    @bet_condition = config['pinnacle']['bet_condition']

    @driver = login
    @main_window = {
      id: 'main',
      handle: @driver.window_handles.first
    }
    @windows = []

    @logger.info("PinnacleBetter initialized")
  end

  def start()
    @logger.info("Start monitoring odds")
    begin
      loop do
        live_tenis_list = get_live_tenis_list

        @windows.each do |w|
          live_tenis = live_tenis_list.find {|t| t[:url] == w[:url]}
          unless live_tenis
            close_window(w)
          end
        end

        live_tenis_list.each do |t|
          window = @windows.find {|w| t[:url] == w[:url]}
          unless window
            open_window(t[:url])
          end
        end

        @windows.each do |w|
          @driver.switch_to.window(w[:handle])
          @driver.execute_script("PSDYNAMICLINES.UpdateLinesClick()")
          sleep 1
          lines = get_lines
          next if lines.empty?

          lines_to_bet = []
          lines.each_cons(2) do |l1, l2|
            next if l1[:id] != l2[:id]
            next if l1[:type] != l2[:type]
            lower = l1[:money_line] < l2[:money_line] ? l1 : l2
            higher = l1[:money_line] > l2[:money_line] ? l1 : l2

            next unless match_condition?(lower, higher)
            @logger.info("Condition mached\nLower line: #{lower}\nHigher line: #{higher}")

            bet_amount = case lower[:type]
            when :game
              @bet_condition['by_game']['bet_amount'].to_f
            when :set
              @bet_condition['by_set']['bet_amount'].to_f
            when :match
              @bet_condition['by_match']['bet_amount'].to_f
            else
              0
            end
            win = (lower[:money_line] * bet_amount) - bet_amount

            next unless  win > 1.0
            @logger.info("Win is higher than $1: #{win}")

            bet_history = CSV.read('bet_history.csv')
            history = bet_history.find {|row| row[0].to_i == lower[:id]}
            next if history
            @logger.info("History not found for this line: #{lower[:id]}")

            lines_to_bet << lower
          end

          next if lines_to_bet.empty?

          begin
            place_bets(lines_to_bet)
          rescue => e
            @logger.error("Error occurred on placing bets.\n#{e.class}: #{e.message}\n#{e.backtrace.join("\n")}")
          end

          @driver.navigate.refresh
        end

        switch_to_main
        sleep 5
      end
    rescue => e
      @logger.error("Unexpected error occurred. reset\n#{e.class}: #{e.message}\n#{e.backtrace.join("\n")}")
      reset
      retry
    ensure
      @driver.quit
      @bet_history.close
    end
  end

  private
  def login(retry_count: 5)
    count = 0
    begin
      url = "https://www.pinnacle.com/ja"
      ua = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.90 Safari/537.36"
      caps = Selenium::WebDriver::Remote::Capabilities.chrome("chromeOptions" => {args: ["--user-agent=#{ua}", "--window-size=1920,2560", "--blink-settings=imagesEnabled=false", "--lang=ja-JP"]})
      driver = Selenium::WebDriver.for :chrome, desired_capabilities: caps
      driver.manage.timeouts.implicit_wait = 5

      driver.navigate.to url
      driver.find_element(:name, 'CustomerId').send_keys(@id)
      driver.find_element(:name, 'Password').send_keys(@pw)
      driver.find_element(:id, 'loginControlsButton').click()

      login_nexts = driver.find_elements(:class, 'loginNext')
      unless login_nexts.empty?
        login_nexts.first.click()
      end

      Selenium::WebDriver::Wait.new(:timeout => 10).until do
        driver.find_element(:id, 'logoutbtn')
      end

      @logger.info("PinnacleBetter login Succeed")
      return driver
    rescue => e
      count += 1
      retry if retry_count > count
      raise e
    end
  end

  def reset
    @driver.quit if @driver
    @driver = login
    @main_window = {
      id: 'main',
      handle: @driver.window_handles.first
    }
    @windows = []
  end

  def switch_to_main
    @driver.switch_to.window(@main_window[:handle])
  end

  def open_window(url)
    switch_to_main
    current_handles = @driver.window_handles

    @driver.execute_script( "window.open(\"#{url}\")" )

    loop do
      if @driver.window_handles.size > current_handles.size
        @windows << {
          handle: @driver.window_handles.last,
          url: url
        }
        break
      end
      sleep 0.1
    end
    switch_to_main
  end

  def close_window(window)
    begin
      @driver.switch_to.window(window[:handle])
      Selenium::WebDriver::Wait.new(:timeout => 10).until do
        @driver.window_handle == window[:handle]
      end
      @driver.close
    rescue Selenium::WebDriver::Error::NoSuchWindowError
      @logger.debug("Window #{window} seems alredy closed. ignore")
    end

    @windows.delete_if {|w| w[:url] == window[:url]}
    switch_to_main
    @logger.info("Closed window #{window[:handle]}")
  end

  def get_live_tenis_list
    switch_to_main
    list = []
    begin
      @driver.find_element(:xpath, "//a[contains( ./text() , \"#{@id}\" )]")
      live_tenis = @driver.find_elements(:xpath, '//div[contains( ./text() , "LIVE Tennis" )]')
      unless live_tenis.empty?
        live_tenis_a_list = live_tenis.first.find_element(:xpath, '..').find_elements(tag_name: 'a')
        live_tenis_a_list.each do |a|
          href = a.attribute('href')
          list << {
            url: URI.join(@driver.current_url, href)
          }
        end
      end
    rescue
    end
    @driver.navigate.refresh
    list
  end

  def get_lines
    lines = []
    begin
      Selenium::WebDriver::Wait.new(:timeout => 10).until do
        @driver.find_element(id: 'refreshNowUpper').displayed?
      end
      begin
        doc = Oga.parse_html(@driver.page_source)
        lines_div_container = doc.at_css("#linesDiv_container")
        table_linesTbl = lines_div_container.css("table.linesTbl")
        table_linesTbl.each do |table|
          tr_list = table.css("tr")
          tr_now = nil
          tr_list.each do |tr|
            tr_now = tr
            tr_id = tr.attribute('id').value if tr.attribute('id')
            if tr_id =~ /\d+_\d+_\d+/
              next unless tr.at_css("span#divM#{tr_id}")
              next unless tr.at_css("span#divM#{tr_id}").text.strip =~ /\d/
              id = tr_id.split('_')[1].to_i
              rot_id = tr.at_css('td.RotID').text.to_i
              team_id = tr.at_css('td.teamId').at_css('span').at_css('span').text
              money_line = tr.at_css("span#divM#{tr_id}").text.to_f
              type = case team_id
              when /[\w ]+ Game \d+ of Set \d+/
                :game
              when /[\w ]+ To Win Set \d+/
                :set
              when /[\w ]+/
                :match
              else
                nil
              end

              lines << { id: id, tr_id: tr_id, rot_id: rot_id, team_id: team_id, money_line: money_line, type: type }
            end
          end
        end
      rescue => e
        @logger.debug("Something wrong on reading lines. ignore\n#{e.class}: #{e.message}\n#{e.backtrace.join("\n")}")
        @logger.debug("tr: #{tr_now.to_xml}")
        @logger.debug("lines_div_container: #{lines_div_container.to_xml}")
      end
    rescue
    end

    @driver.navigate.refresh if lines.empty?

    lines
  end

  def clear_all_placed_bets()
    @driver.execute_script("jQuery('input.ValueObject').each(function(i, e) {jQuery(e)[0].value = \"\"})")
  end

  def place_bets(lines)
    clear_all_placed_bets
    lines.each do |line|
      bet_amount = case line[:type]
      when :game
        @bet_condition['by_game']['bet_amount'].to_f
      when :set
        @bet_condition['by_set']['bet_amount'].to_f
      when :match
        @bet_condition['by_match']['bet_amount'].to_f
      else
        next
      end

      input_id = "M#{line[:tr_id]}"
      @driver.execute_script("jQuery('input##{input_id}')[0].value = #{bet_amount}")
    end
    @driver.execute_script("PSSTRAIGHTWAGERS.SubmitWagers()")
    Selenium::WebDriver::Wait.new(:timeout => 3).until do
      @driver.find_element(css: 'div#ShowBetAll').displayed?
    end
    sleep 1

    doc = Oga.parse_html(@driver.page_source)
    commit_button = doc.at_css('button.ConfirmationButton')

    raise 'Commit button not found' unless commit_button

    @driver.find_element(css: 'button.ConfirmationButton').click

    Selenium::WebDriver::Wait.new(:timeout => 3).until do
      @driver.find_element(css: 'button.CheckBets').displayed?
    end

    begin
      doc = Oga.parse_html(@driver.page_source)
      tables = doc.css('table.ticket1')
      table = tables.find do |t|
        h3 = t.at_css('h3')
        if h3
          h3.text == "待機中のベット"
        else
          false
        end
      end

      raise '待機中のベット table not found' unless table

      table.css('tr').each do |tr|
        next unless tr.attribute('class')
        tr_class = tr.attribute('class').value
        if tr_class.start_with?("BetFor")
          id = tr_class.split('_')[1].to_i
          line = lines.find {|line| line[:id] == id}
          @bet_history << [line[:id], line[:team_id], line[:money_line]]
        end
      end
    rescue => e
      @logger.error("Unexpected error occurred while checking successful lines.\n#{e.class}: #{e.message}\n#{e.backtrace.join("\n")}")
      lines.each do |line|
        @bet_history << [line[:id], line[:team_id], line[:money_line]]
      end
      raise e
    end
  end

  def match_condition?(lower, higher)
    type = lower[:type]
    bet_condition = case type
    when :game
      @bet_condition['by_game']
    when :set
      @bet_condition['by_set']
    when :match
      @bet_condition['by_match']
    else
      return false
    end

    lower_condition = bet_condition['lower']
    higher_condition = bet_condition['higher']

    lower[:money_line] >= lower_condition['min'] && lower[:money_line] <= lower_condition['max'] && higher[:money_line] >= higher_condition['min']
  end
end
