1. インストール手順

1-1. rubyのインストール
以下の手順に従いruby 2.2.6をインストールしてください。
http://qiita.com/shimoju/items/41035b213ad0ac3a979e

1-2. ChromeDriverのインストール
以下のページからChromeDriverをダウンロードし、
rubyコマンドのバイナリがあるパスに設置してください。
(C:\Ruby22-x64\binなど)

https://sites.google.com/a/chromium.org/chromedriver/downloads

1-3. 依存gemのインストール
コマンドプロンプトから以下のコマンドを実行。

---
> cd <このファイルがあるディレクトリ>
> bundle install --path vendor/bundle
---

1-4. 設定ファイルの修正
config.ymlのidとpwを自分のアカウントのものに変えてください。


2. 使用手順
コマンドプロンプトから以下のコマンドを実行。

---
> cd <このファイルがあるディレクトリ>
> bundle exec ruby app.rb
---
