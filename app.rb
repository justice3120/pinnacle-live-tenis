require 'yaml'
require './lib/pinnacle_better'

config = YAML.load_file('./config.yml')

pinnacle = PinnacleBetter.new(config)
pinnacle.start()
